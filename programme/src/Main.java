import fr.univ.smb.sirambd.aly.console.Principale;
import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;

/**
 * Ici on a le lancement de l'application, cependant on a fait que le console
 * Donc c'est le console qui sera lancer! Et Oui en effet sa reviens à la même chose
 * que le lancement qui se trouve dans le package console.
 * @see Principale
 */
public class Main {

    public static void main(String[] args) throws CarteNotFoundException, InterruptedException {
        Principale.main(args);
    }
}
