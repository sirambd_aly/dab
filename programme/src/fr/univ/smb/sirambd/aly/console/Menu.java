package fr.univ.smb.sirambd.aly.console;

import fr.univ.smb.sirambd.aly.etats.*;
import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;
import fr.univ.smb.sirambd.aly.models.*;
import fr.univ.smb.sirambd.aly.utilities.ConsoleColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Cette classe gère le console, et permet de traiter les demandes de l'utilisateur
 */
public class Menu {
    // Attributes
    private Scanner scanner;
    private Banque laposte;
    private Distrib distrib;
    private Carte carte ;
    private CarteClient carteClient;

    // Constructors
    public Menu() {
        List<CarteClient> carteClients = new ArrayList<>();
        Client client = new Client();
        client.ajouteCompte(new Compte("A0123695C",650,300));
        client.ajouteCompteDestinaraire(new CompteDestinataire("17596325875G"));
        client.ajouteCompteDestinaraire(new CompteDestinataire("17552329678A"));
        carte = new Carte("1234567890", "0000");
        carteClient = new CarteClient(carte.getNoCarte(),client);
        carteClients.add(carteClient);
        scanner = new Scanner(System.in);
        laposte = new Banque("LA POSTE", "LAPOSTE",carteClients);

        distrib = new Distrib(laposte);

        distrib.insererCarte(carte);
    }

    // Methods
    private void init() throws InterruptedException{
        System.out.println("-----------------------------");
        System.out.println("-----ETAT ATTENTE CARTE------");
        System.out.println("-----------------------------");
        System.out.println("Veuillez inserer votre carte");
        Thread.sleep(1000);
        System.out.println("-----------------------------");
        System.out.println("-----ETAT ATTENTE CODE------");
        System.out.println("-----------------------------");
        Thread.sleep(1000);
        System.out.println("Veuillez saisir votre code");
        Thread.sleep(1000);
        System.out.print("*");
        Thread.sleep(100);
        System.out.print("*");
        Thread.sleep(100);
        System.out.print("*");
        Thread.sleep(100);
        System.out.println("*");
        Thread.sleep(300);
        System.out.println("Code Bon");
    }
    private int demandechoix(String question){
        System.out.println(question);
        return scanner.nextInt();
    }
    private void principaleInfo(){
        System.out.println("1. Effectué un virement");
        System.out.println("2. Consulter vos comptes");
        System.out.println("3. quitter");
    }

    private void afficheEtat(String nomEtat){
        System.out.println(ConsoleColor.ANSI_YELLOW );
        System.out.println("-----------------------------");
        System.out.println( nomEtat);
        System.out.println("-----------------------------");
        System.out.println(ConsoleColor.ANSI_RESET);
    }

    private void verification(String message,int temps) throws InterruptedException {
        System.out.println(ConsoleColor.ANSI_GREEN );
        System.out.println(message);
        for (int i=0;i<=temps;i++){
            System.out.print(".");
            Thread.sleep(200);
        }
        System.out.println(ConsoleColor.ANSI_RESET);
    }

    private Compte detailcompte() throws CarteNotFoundException {
        int choix;
        afficheEtat(distrib.getEtat().getNomEtat());
        distrib.setEtat(new AttenteListeComptes(distrib));
        afficheEtat(distrib.getEtat().getNomEtat());
        List<Compte> listcompte = distrib.getEtat().listeCompte();
        do {
            distrib.setEtat(new afficheListeCompte(distrib));
            afficheEtat(distrib.getEtat().getNomEtat());
            System.out.println(ConsoleColor.ANSI_BLUE);
            distrib.getEtat().afficherListeComptes();
            System.out.println(ConsoleColor.ANSI_RESET);
            choix = demandechoix("Choisissez votre compte:");
        } while(choix>listcompte.size()+1 || choix< 1);
        return listcompte.get(choix-1);
    }

    public void principale() throws InterruptedException, CarteNotFoundException {
        init();
        boolean quitter = false;
        int choix;
        float montant;
        Compte compteClient;
        List<CompteDestinataire> compteDestinatairesClient;
        distrib.setEtat(new AttenteOperation(distrib));
        System.out.println(distrib.getEtat().getNomEtat());
        while (!quitter){
            System.out.println(ConsoleColor.ANSI_BLUE);
            distrib.getEtat().choisirOperation();
            System.out.println(ConsoleColor.ANSI_RESET);
            choix = demandechoix("Choisissez une opération:");
            switch (choix){
                case 1:
                    compteClient = detailcompte();
                    verification("Veuillez patienter svp",5);
                    distrib.setEtat(new AfficheDetailsCompte(distrib));
                    afficheEtat(distrib.getEtat().getNomEtat());
                    distrib.getEtat().afficheDetailCompte(compteClient);
                    Thread.sleep(1000);
                    break;
                case 2:
                    System.out.println(ConsoleColor.ANSI_YELLOW + "Cette fonctionnalité n'est pas encore disponible!" + ConsoleColor.ANSI_RESET);
                    // TODO: Faire un retrait
                    break;
                case 3:
                    compteClient = detailcompte();
                    distrib.setEtat(new AttenteListeComptesDestinataire(distrib));
                    afficheEtat(distrib.getEtat().getNomEtat());
                    compteDestinatairesClient = distrib.getEtat().listeDestinataire();
                    do{
                        distrib.setEtat(new AfficheListeDestinataire(distrib));
                        afficheEtat(distrib.getEtat().getNomEtat());
                        System.out.println(ConsoleColor.ANSI_BLUE);
                        distrib.getEtat().afficheListeDestinataire(compteDestinatairesClient);
                        System.out.println(ConsoleColor.ANSI_RESET);
                        choix = demandechoix("Choisissez un compte destinataire:");
                    }while (choix > compteDestinatairesClient.size()+1 || choix < 1);
                    System.out.println("veuillez saisir le montant du virement");
                    montant=scanner.nextFloat();
                    verification("Verification en cours",10);
                    if(distrib.getEtat().faireUnVirement(compteClient,compteDestinatairesClient.get(choix-1).getRib(),montant)){
                        distrib.setEtat(new VirementEffectuer(distrib));
                        afficheEtat(distrib.getEtat().getNomEtat());
                    }else{
                        distrib.setEtat(new virementPasOk(distrib));
                        afficheEtat(distrib.getEtat().getNomEtat());
                    }
                    distrib.setEtat(new AfficheDetailsCompte(distrib));
                    afficheEtat(distrib.getEtat().getNomEtat());
                    distrib.getEtat().afficheDetailCompte(compteClient);
                    Thread.sleep(1000);
                    break;
                case 4:
                    System.out.println("Merci à vous pour votre visite à bientôt!");
                    quitter = true;
                    break;
                default :
                    System.out.println("Nous n'avons pas compris votre demande, veuillez recommencer!");
            }
        }
    }
}
