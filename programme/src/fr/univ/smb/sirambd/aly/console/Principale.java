package fr.univ.smb.sirambd.aly.console;

import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;

/**
 * Ici on a le lancement du console
 */
public class Principale {
    private static Menu menu = new Menu();

    public static void main(String[] args) throws InterruptedException, CarteNotFoundException {
        menu.principale();
    }
}