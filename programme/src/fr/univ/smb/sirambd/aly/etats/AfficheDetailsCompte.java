package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.Compte;
import fr.univ.smb.sirambd.aly.models.Distrib;


public class AfficheDetailsCompte extends Etat {

    public AfficheDetailsCompte(Distrib distrib_gap) {
        super(distrib_gap, "Affiche les détails de compte");
    }

    public void afficheDetailCompte(Compte noCompte) {
        super.afficheDetailCompte(noCompte);
    }



}
