package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.CompteDestinataire;
import fr.univ.smb.sirambd.aly.models.Distrib;

import java.util.List;



public class AfficheListeDestinataire extends Etat {

    public AfficheListeDestinataire(Distrib distrib_gap) {
        super(distrib_gap, "Affiche la liste des destinataires");
    }

    public void afficheListeDestinataire() {
        super.afficheListeDestinataire();
    }

    public void afficheListeDestinataire(List<CompteDestinataire> compteDestinataireList) {
        super.afficheListeDestinataire(compteDestinataireList);
    }


}
