package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.Distrib;


public class AttenteCarte extends Etat {

    public AttenteCarte(Distrib distrib_gap) {
        super(distrib_gap, "Attente d'une carte");
    }

    public void insererCarte(String noCarte) {
        super.insererCarte(noCarte);
    }



}
