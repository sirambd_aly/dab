package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;
import fr.univ.smb.sirambd.aly.models.Compte;
import fr.univ.smb.sirambd.aly.models.Distrib;

import java.util.List;

public class AttenteListeComptes extends Etat {

    public AttenteListeComptes(Distrib distrib_gap) {
        super(distrib_gap, "Affiche la liste des comptes");
    }

    public List<Compte> listeCompte() throws CarteNotFoundException {
        return super.listeCompte();
    }


}
