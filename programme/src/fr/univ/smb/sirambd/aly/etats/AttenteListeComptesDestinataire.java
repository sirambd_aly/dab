package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.CompteDestinataire;
import fr.univ.smb.sirambd.aly.models.Distrib;

import java.util.List;

public class AttenteListeComptesDestinataire extends Etat {

    public AttenteListeComptesDestinataire(Distrib distrib_gap) {
        super(distrib_gap, "Affiche la liste des comptes");
    }

    public List<CompteDestinataire> listeDestinataire() {
        return super.listeDestinataire();
    }


}
