package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.Distrib;


public class AttenteOperation extends Etat {

    public AttenteOperation(Distrib distrib_gap) {
        super(distrib_gap, "Attente d'une opération");
    }

    public void choisirOperation() {
        super.choisirOperation();
    }

}
