package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;
import fr.univ.smb.sirambd.aly.models.Compte;
import fr.univ.smb.sirambd.aly.models.CompteDestinataire;
import fr.univ.smb.sirambd.aly.models.Distrib;

import java.util.List;


public class Etat {
    // Attributes
    private String nomEtat;

    // Constructors
    public Distrib distrib;

    public Etat(Distrib distrib_gap, String nomEtat) {
        this.distrib = distrib_gap;
        this.nomEtat = nomEtat;
    }

    // Methods
    public void insererCarte(String noCarte) {
        distrib.setNoCarte(noCarte);
    }

    public Compte choisirCompte(Compte numCompte) {
        return distrib.getBanquedeRattachement().getCompte(numCompte);
    }

    public boolean faireUnVirement(Compte noCompte, String RIB, float montant) {
        return distrib.getBanquedeRattachement().virementOk(noCompte, RIB, montant);
    }

    public void afficherListeComptes(List<Compte> listeCompte) {
        for (int i = 0; i < listeCompte.size(); i++)
            System.out.println(i + 1 + " - " + listeCompte.get(i).getNoCompte());
    }

    public void afficherListeComptes() throws CarteNotFoundException {
        List<Compte> listeCompte = this.listeCompte();
        for (int i = 0; i < listeCompte.size(); i++)
            System.out.println(i + 1 + " - " + listeCompte.get(i).getNoCompte());
    }

    public void afficheDetailCompte(Compte noCompte) {
        Compte comptechoisi = choisirCompte(noCompte);
        System.out.println(" NUMERO COMPTE: " + comptechoisi.getNoCompte());
        System.out.println(" SOLDE " + comptechoisi.getSolde());
        System.out.println(" PLAFOND DE RETRAIT: " + comptechoisi.getPlafondRetrait());
    }

    public void afficheListeDestinataire(List<CompteDestinataire> compteDestinataireList) {
        for (int i = 0; i < compteDestinataireList.size(); i++)
            System.out.println(i + 1 + " - " + compteDestinataireList.get(i).getRib());
    }

    public void choisirOperation() {
        System.out.println("1 - Consultation");
        System.out.println("2 - retrait");
        System.out.println("3 - Virement");
        System.out.println("4 - Quitter");
    }
/*
    public boolean saisirCode(int codeSaisi) {
    }*/

    public void afficheListeDestinataire() {
        List<CompteDestinataire> cd = this.listeDestinataire();
        for (int i = 0; i < cd.size(); i++) {
            System.out.println(cd.get(i).getRib());
        }
    }

    public List<CompteDestinataire> listeDestinataire() {
        return distrib.getBanquedeRattachement().getListeComptesDestinataire(distrib.getNoCarte());
    }

    public List<Compte> listeCompte() throws CarteNotFoundException {
        return distrib.getBanquedeRattachement().getListeCompte(distrib.getNoCarte());
    }

    // Getters
    public String getNomEtat() {
        return nomEtat;
    }

}
