package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.Compte;
import fr.univ.smb.sirambd.aly.models.Distrib;


public class VirementEffectuer extends Etat {
    public Distrib distrib;

    public VirementEffectuer(Distrib distrib_gap) {
        super(distrib_gap, "Virement effectué!");
    }

    public boolean faireUnVirement(Compte noCompte, String RIB, float montant) {
        return super.faireUnVirement(noCompte,RIB,montant);
    }



}
