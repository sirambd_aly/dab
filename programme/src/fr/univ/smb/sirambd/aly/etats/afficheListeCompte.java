package fr.univ.smb.sirambd.aly.etats;

import java.util.List;

import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;
import fr.univ.smb.sirambd.aly.models.Compte;
import fr.univ.smb.sirambd.aly.models.Distrib;


public class afficheListeCompte extends Etat {

    public afficheListeCompte(Distrib distrib_gap) {
        super(distrib_gap, "Affiche la liste de comptes");
    }

    public void afficherListeComptes(List<Compte> listeCompte) {
        super.afficherListeComptes(listeCompte);
    }

    public void afficherListeComptes() throws CarteNotFoundException {
        super.afficherListeComptes();
    }



}
