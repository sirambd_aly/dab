/**
 * Contient tous les états du distributeur de billet
 * @author Abdourahamane BOINAIDI et Aly-Nayef BASMA
 */
package fr.univ.smb.sirambd.aly.etats;