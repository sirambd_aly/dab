package fr.univ.smb.sirambd.aly.etats;

import fr.univ.smb.sirambd.aly.models.Compte;
import fr.univ.smb.sirambd.aly.models.Distrib;


public class virementPasOk extends Etat {

    public Distrib distrib;

    public virementPasOk(Distrib distrib_gap) {
        super(distrib_gap, "Virement refusé!");
    }

    public boolean faireUnVirement(Compte noCompte, String RIB, float montant) {
        return super.faireUnVirement(noCompte, RIB, montant);
    }


}
