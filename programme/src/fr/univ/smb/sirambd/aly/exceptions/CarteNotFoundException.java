package fr.univ.smb.sirambd.aly.exceptions;

public class CarteNotFoundException extends Exception {
    public CarteNotFoundException() {
        System.err.println("La carte demandé n'a pas été trouver");
    }
}