/**
 * On regroupe ici tous nos exceptions utiles pour déboguer l'application
 * @author Abdourahamane BOINAIDI et Aly-Nayef BASMA
 */
package fr.univ.smb.sirambd.aly.exceptions;