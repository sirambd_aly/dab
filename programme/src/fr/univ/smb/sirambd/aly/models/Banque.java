package fr.univ.smb.sirambd.aly.models;

import fr.univ.smb.sirambd.aly.exceptions.CarteNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static fr.univ.smb.sirambd.aly.models.NatureOperation.virement;

public class Banque {
    // Attributes
    private String nomBanque;
    private String codeBanque;
    private List<Banque> autresBanques = new ArrayList<Banque>();
    private List<CarteClient> carteClients = new ArrayList<>();
    private List<Client> mesClients = new ArrayList<>();

    // Constructors
    public Banque(String nomBanque, String codeBanque) {
        this.nomBanque = nomBanque;
        this.codeBanque = codeBanque;
    }

    public Banque(String nomBanque, String codeBanque,List<CarteClient> carteClients ) {
        this.nomBanque = nomBanque;
        this.codeBanque = codeBanque;
        this.carteClients = carteClients;
    }

    // Methods
    public boolean estUnClient(String noCarte) {
        return true;
    }

    /**
     * Recherche la carte donnée en paramètre
     *
     * @param noCarte
     * @return L'indice de la carte ou sinon -1 si aucun carte n'a été trouver
     */
    public int rechercheCarte(String noCarte) {
        for (int i = 0; i < carteClients.size(); i++) {
            if (carteClients.get(i).esTuCarte(noCarte)) return i;
        }
        return -1;
    }

    public Client recupereClient(int indiceCarte) {
        CarteClient carteClient = carteClients.get(indiceCarte);
        return carteClient.monClient();
    }

    public List<Compte> getListeCompte(String noCarte) throws CarteNotFoundException {
        int indiceCarte = rechercheCarte(noCarte);
        if (indiceCarte == -1) throw new CarteNotFoundException();
        Client client = recupereClient(indiceCarte);
        return client.mesComptes();
    }

    public List<CompteDestinataire> getListeComptesDestinataire(String noCarte) {
        // Recherche de la carte client
        CarteClient carteClient = null;
        for (CarteClient cc : carteClients) {
            if (cc.esTuCarte(noCarte)) carteClient = cc;
        }
        // Récupération de la liste de comptes destinataire
        if (carteClient != null) {
            return carteClient.monClient().mesComptesDestinataire();
        }
        // Aucun carte client n'a été trouver
        return null;
    }

    public CompteDestinataire getCompteDestinataire(String rib, String noCarte) {
        // On récupère tous les comptes destinataires du client
        List<CompteDestinataire> compteDestinataires = getListeComptesDestinataire(noCarte);
        // On cherche Le compte destinataire correspondant au rib
        for (CompteDestinataire cd : compteDestinataires) {
            if (cd.esTu(rib)) return cd; // On retourne le compte destinataire courant si son rib correspond
        }
        // Aucun compte destinataire a été trouver
        return null;
    }

    public boolean montantDisponible(float montant) {
        return false;
    }

    public boolean virementOk(Compte noCompte, String RIB, float montant) {
        if (noCompte.getSolde()>=montant && noCompte.getPlafondRetrait()>=montant){
            noCompte.reduireSolde(montant,virement);
            return true;
        }
        return false;
    }

    /**
     * Ajouter une autre banque
     *
     * @param banque
     */
    public void ajouteBanque(Banque banque) {
        this.autresBanques.add(banque);
    }

    public void ajouteCarteClient(CarteClient cc) {
        this.carteClients.add(cc);
    }

    public void ajouteClient(Client client) {
        this.mesClients.add(client);
    }

    // Setters
    public void setNomBanque(String value) {
        this.nomBanque = value;
    }

    public void setCodeBanque(String value) {
        this.codeBanque = value;
    }

    // Getters
    public Compte getCompte(Compte noCompte) {
        return noCompte;
    }

    public String getCodeBanque() {
        return this.codeBanque;
    }

    public String getNomBanque() {
        return this.nomBanque;
    }
}