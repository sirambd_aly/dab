package fr.univ.smb.sirambd.aly.models;


public class Carte {
    // Attributes
    private String noCarte;
    private String code;
    private int nbEssaisRestant;

    // Constructors
    public Carte(String noCarte, String code) {
        this.noCarte = noCarte;
        this.code = code;
        this.nbEssaisRestant = 3;
    }

    public Carte(String noCarte, String code, int nbEssaisRestant) {
        this.noCarte = noCarte;
        this.code = code;
        this.nbEssaisRestant = nbEssaisRestant;
    }

    // Methods
    public boolean codeOK(String codeS) {
        return this.code == codeS;
    }

    // Setters
    public void setNoCarte(String value) {
        this.noCarte = value;
    }
    public void setNbEssaisRestant(int value) {
        this.nbEssaisRestant = value;
    }
    public void setCode(String value) {
        this.code = value;
    }

    // Getters
    public String getNoCarte() {
        return this.noCarte;
    }
    public int getNbEssaisRestant() {
        return this.nbEssaisRestant;
    }
    public String getCode() {
        return this.code;
    }
}
