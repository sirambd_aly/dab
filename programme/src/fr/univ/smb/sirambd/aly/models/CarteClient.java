package fr.univ.smb.sirambd.aly.models;

public class CarteClient {
    // Attributes
    private String noCarte;
    private Client monClient;

    // Constructors
    public CarteClient(String noCarte, Client monClient) {
        this.noCarte = noCarte;
        this.monClient = monClient;
    }

    // Methods
    public boolean esTuCarte(String noCarteClient) { return this.noCarte.equals(noCarteClient); }
    public Client monClient() {
        return this.monClient;
    }

    // Setters
    private void setNoCarte(String value) {
        this.noCarte = value;
    }

    // Getters
    private String getNoCarte() {
        return this.noCarte;
    }
}
