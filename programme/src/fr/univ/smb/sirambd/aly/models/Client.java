package fr.univ.smb.sirambd.aly.models;

import java.util.ArrayList;
import java.util.List;


public class Client {
    // Attributes
    private List<Compte> mesComptes;
    private List<CompteDestinataire> mesComptesDestinataires;

    // Constructors
    public Client() {
        this.mesComptes = new ArrayList<>();
        this.mesComptesDestinataires = new ArrayList<>();
    }
    public Client(ArrayList<Compte> comptes) {
        this.mesComptes = comptes;
        this.mesComptesDestinataires = new ArrayList<>();
    }
    public Client(ArrayList<Compte> comptes, ArrayList<CompteDestinataire> compteDestinataires) {
        this.mesComptes = comptes;
        this.mesComptesDestinataires = compteDestinataires;
    }

    // Methods
    public void ajouteCompte(Compte compte) {
        this.mesComptes.add(compte);
    }
    public void ajouteCompteDestinaraire(CompteDestinataire compteDestinataire){
        this.mesComptesDestinataires.add(compteDestinataire);
    }
    public String toString() {
        String res = "- Client\n" +
                "\t- Mes comptes:\n";
        for (Compte c : mesComptes()){
            res += "\t\t- " + c + "\n";
        }
        res += "\t- Mes destinataires:\n";
        for (CompteDestinataire cd : mesComptesDestinataire()){
            res += "\t\t- " + cd + "\n";
        }
        return res;
    }
    public List<Compte> mesComptes() {
        return this.mesComptes;
    }
    public List<CompteDestinataire> mesComptesDestinataire() {
        return this.mesComptesDestinataires;
    }
}
