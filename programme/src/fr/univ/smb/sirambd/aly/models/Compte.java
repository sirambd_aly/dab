package fr.univ.smb.sirambd.aly.models;


public class Compte {
    // Attributes
    private String noCompte;
    private float solde;
    private float plafondRetrait;

    // Constructors
    public Compte(String noCompte, float solde, float plafondRetrait) {
        this.noCompte = noCompte;
        this.solde = solde;
        this.plafondRetrait = plafondRetrait;
    }

    // Methods
    public boolean reduireSolde(float montant, NatureOperation natureOperation) {
        this.setSolde(getSolde()-montant);
        return true;
    }
    public String toString() {
        return "Je suis le compte n° " + getNoCompte() + ", avec un solde de " + getSolde() + "€";
    }

    // Setters
    public void setSolde(float value) {
        this.solde = value;
    }
    public void setNoCompte(String value) {
        this.noCompte = value;
    }
    public void setPlafondRetrait(float value) {
        this.plafondRetrait = value;
    }

    // Getters
    public float getSolde() {
        return this.solde;
    }
    public String getNoCompte() {
        return this.noCompte;
    }
    public float getPlafondRetrait() {
        return this.plafondRetrait;
    }
}
