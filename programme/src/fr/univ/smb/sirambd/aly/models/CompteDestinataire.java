package fr.univ.smb.sirambd.aly.models;

public class CompteDestinataire {
    // Attributes
    private String rib;

    // Constructors
    public CompteDestinataire(String rib) {
        this.rib = rib;
    }

    // Methods
    public String toString() {
        return "Je suis le compte destinataire avec comme rib " + getRib();
    }
    public boolean esTu(String rib){
        return getRib() == rib;
    }

    // SETTERS
    public void setRib(String value) {
        this.rib = value;
    }

    // GETTERS
    public String getRib() {
        return this.rib;
    }
}
