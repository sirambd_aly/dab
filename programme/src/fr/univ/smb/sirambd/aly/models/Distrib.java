package fr.univ.smb.sirambd.aly.models;
import fr.univ.smb.sirambd.aly.etats.Etat;


public class Distrib {
    // Attributes
    private int nbBillets10;
    private int nbBillets20;
    private int nbBillets50;
    private int nbBillets100;
    private String noCarte;
    private Banque banqueDeRattachement;
    private Carte carteInseree;
    private Etat etat;

    // Constructors
    public Distrib(String noCarte, Banque banqueDeRattachement, Etat etat) {
        this.nbBillets10 = 100;
        this.nbBillets20 = 40;
        this.nbBillets50 = 25;
        this.nbBillets100 = 10;
        this.noCarte = noCarte;
        this.banqueDeRattachement = banqueDeRattachement;
        this.etat = etat;
    }
    public Distrib(Banque banqueDeRattachement) {
        this.nbBillets10 = 100;
        this.nbBillets20 = 40;
        this.nbBillets50 = 25;
        this.nbBillets100 = 10;
        this.banqueDeRattachement = banqueDeRattachement;
        this.etat = etat;
    }

    // Methods
    public void insererCarte(Carte carte){
        this.noCarte = carte.getNoCarte();
        this.carteInseree = carte;
    }
    // Setters
    public void setNbBillets10(int value) { this.nbBillets10 = value; }
    public void setNbBillets20(int value) { this.nbBillets20 = value; }
    public void setNbBillets50(int value) { this.nbBillets50 = value; }
    public void setNbBillets100(int value) { this.nbBillets100 = value; }
    public void setNoCarte(String value) { this.noCarte = value; }
    public void setCarteInseree(Carte value) { this.carteInseree = value; }
    public void setBanquedeRattachement(Banque banqueDeRattachement) { this.banqueDeRattachement = banqueDeRattachement; }
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    // Getters
    public int getNbBillets10() { return this.nbBillets10; }
    public int getNbBillets20() { return this.nbBillets20; }
    public int getNbBillets50() { return this.nbBillets50; }
    public int getNbBillets100() { return this.nbBillets100; }
    public String getNoCarte() { return this.noCarte; }
    public Carte getCarteInseree() { return this.carteInseree; }
    public Banque getBanquedeRattachement() { return this.banqueDeRattachement; }
    public Etat getEtat() { return this.etat; }
}
