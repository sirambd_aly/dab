package fr.univ.smb.sirambd.aly.models;


public enum NatureOperation {
    retrait,
    virement,
    consultation;
}
