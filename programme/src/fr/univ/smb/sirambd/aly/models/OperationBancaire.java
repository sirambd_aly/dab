package fr.univ.smb.sirambd.aly.models;

import java.util.Date;


public class OperationBancaire {


    private NatureOperation nature;


    private NatureOperation getNature() {
        // Automatically generated method. Please do not modify this code.
        return this.nature;
    }


    private void setNature(NatureOperation value) {
        // Automatically generated method. Please do not modify this code.
        this.nature = value;
    }


    private float Montant;


    private float getMontant() {
        // Automatically generated method. Please do not modify this code.
        return this.Montant;
    }


    private void setMontant(float value) {
        // Automatically generated method. Please do not modify this code.
        this.Montant = value;
    }


    private Date dateOperation;


    private Date getDateOperation() {
        // Automatically generated method. Please do not modify this code.
        return this.dateOperation;
    }


    private void setDateOperation(Date value) {
        // Automatically generated method. Please do not modify this code.
        this.dateOperation = value;
    }


    public boolean setOperation(NatureOperation nature, float Montant, Date date) {
        return true;
    }

}
