/**
 * Ceci est notre package principale, qui contient tous les autres packages de l'application
 * Nous regroupons ici tous les packages utile pour le dévéloppement de l'application
 * Vous pouvez cependant remarquer que les packages sont spécifiques aux classes
 * Car sa nous permet de mieux organiser notre code.
 *
 * <b>Merci à vous pour votre lecture!!</b>
 *
 * Ce fût un plaisir pour nous d'avoir dévélopper cet application.
 * @author Abdourahamane BOINAIDI et Aly-Nayef BASMA
 * @version 1.0.0
 * @see [http://wwww.univ-smb.fr](université savoie mont blanc)
 */
package fr.univ.smb.sirambd.aly;