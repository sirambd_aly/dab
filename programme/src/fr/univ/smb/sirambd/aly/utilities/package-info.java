/**
 * Nous regroupons ici, tous les classes utiles pour mieux organiser l'application
 * @author Abdourahamane BOINAIDI et Aly-Nayef BASMA
 */
package fr.univ.smb.sirambd.aly.utilities;